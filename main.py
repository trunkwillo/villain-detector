import sys
import re

import modules.metadata_retriever as md
import modules.crawler as cr
import modules.face_recognition as fr

def main(args):
    # Argument processing
    skip_download = False
    train_snn = False
    if "-s" in args:
        skip_download = True
    if "-t" in args:
        train_snn = True

    # Get the cast list
    movie_name = "Inglorious Basterds"
    print("Starting to get the cast")
    cast_list = md.getIdWithName(movie_name, 2009, getToken())

    # Limit the cast
    cast_limit = 5
    cast_list = cast_list[:cast_limit]

    # Create a dataset with the cast list
    if not skip_download:
        print("Downloading dataset")
        cr.generateDataset(cast_list)

    # Start up the Siamese Network
    model = fr.SNN()
    cleaned_movie_name = re.sub('[^A-Za-z0-9]+', '', movie_name)
    if train_snn:
        model.train_siamese()
        model.save_siamese("model_" + cleaned_movie_name)
    else:
        model.load_siamese("model_" + cleaned_movie_name)

    model.model.summary()

    # print(model.predict_class("Eli Roth/Image_32.jpg"))
    # model.predict("Brad Pitt/Image_5.jpg", "Brad Pitt/Image_23.jpg",)
    # model.predict("Christoph Waltz/Image_4.jpg", "Mélanie Laurent/Image_3.jpg")
    # model.predict("Christoph Waltz/Image_5.jpg", "Michael Fassbender/Image_4.jpg")
    # model.predict("Eli Roth/Image_4.jpg", "Eli Roth/Image_28.jpg")



def getToken():
    f = open("tmdbtoken.txt", "r")
    return f.read()

main(sys.argv)