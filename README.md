# Villain Detector

Ever wondered if your favorite movie has the greatest villain ever represented? Everything leading to its plot twist is completely unexpected? Not even a robot could see it from a mile away right? Well, this one most likely won't be able to but there's only one way to find out! 

Follow the development of this project here: https://dinisportfolio.com/portfolio/villain-detector/
