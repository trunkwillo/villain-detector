import matplotlib.pyplot as plt
import numpy as np
import os
import random
import tensorflow.compat.v1 as tf 
from pathlib import Path, PureWindowsPath
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import GlobalAveragePooling2D
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import Lambda
from tensorflow.keras import models
from tensorflow.keras.models import load_model

from PIL import Image
#from keras import backend as K
from tensorflow.keras import backend as K

from glob import glob
import itertools as it

IMG_SHAPE = (224, 224, 3)
BATCH_SIZE = 128
EPOCHS = 1000
IMAGES_FOLDER = "dataset_cache/"
MODEL_FOLDER = "assets/"


'''
    Face Recognition
    Main features:
        - Trains a siamese network to do face recognition 

    Arguments:
        Batch Size
        Epochs
        Embedding Dims
        Fine Tune
        Fine Tune Percentage


    Most of the credit to Adrian Rosebrock for the excellent tutorial here: https://www.pyimagesearch.com/2021/01/18/contrastive-loss-for-siamese-networks-with-keras-and-tensorflow/

'''

class SNN:

    def __init__(self):
        self.model = None
        tf.enable_eager_execution(tf.ConfigProto(log_device_placement=False)) 

    '''
        Trains the siamese network with various triplet values.
    '''
    def train_siamese(self):
        # Get image paths (list of lists with all the images of the same actor)
        imagePaths, labels = self.get_filelist()

        # Combine each triplet (for each image, get a similar one and a wrong one)
        pairImages, pairLabels = self.create_pairs(imagePaths) # We'll keep this shape for easier integration in the used example
        pairImages, pairLabels = self.unison_shuffled_copies(pairImages, pairLabels)

        imageCount = len(pairLabels)

        cut = int(0.9 * imageCount)
        pairTrain, pairTest, labelTrain, labelTest = pairImages[:cut], pairImages[cut:], pairLabels[:cut], pairLabels[cut:]
        print("[INFO] Using " + str(cut) + " training examples and " + str(imageCount - cut) + "validation examples.")

        # Building the SNN structure
        imgA = Input(shape= IMG_SHAPE)
        imgB = Input(shape= IMG_SHAPE)
        featureExtractor = self.build_siamese_model(IMG_SHAPE)
        featsA = featureExtractor(imgA)
        featsB = featureExtractor(imgB)

        distance = Lambda(self.euclidean_distance)([featsA, featsB])
        outputs = Dense(1, activation="sigmoid")(distance)
        model = Model(inputs=[imgA, imgB], outputs=outputs)

        opt = tf.keras.optimizers.Adam(lr=0.001)
        model.compile(loss = self.contrastive_loss, optimizer = opt, metrics=["accuracy"])

        history = model.fit([pairTrain[:, 0], pairTrain[:, 1]], labelTrain[:], validation_data=([pairTest[:, 0], pairTest[:, 1]], labelTest[:]), batch_size = BATCH_SIZE, epochs = EPOCHS)

        self.plot_training(history, "plot.png")

        self.model = model
        
        return True

    def get_filelist(self):
        imagePaths = [[f +"/" + i for i in os.listdir(os.getcwd() + '/dataset_cache/' + f)] for f in os.listdir(os.getcwd() + '/dataset_cache')]
        labels = [f for f in os.listdir(os.getcwd() + '/dataset_cache')]
        return imagePaths, labels


    def create_pairs(self, fileNames):
        pairImages = []
        pairLabels = []

        for i in range(len(fileNames)):
            for j in range(len(fileNames[i])):
                currentImage = self.preprocess_image(fileNames[i][j])

                otherPositiveIdx = j
                while otherPositiveIdx == j: # To not grab the same one
                    otherPositiveIdx = np.random.choice(len(fileNames[i]))
                otherPositive = self.preprocess_image(fileNames[i][otherPositiveIdx])

                pairImages.append([currentImage, otherPositive])
                pairLabels.append(1)


                otherNegativeListIdx = i
                while otherNegativeListIdx == i: # To not grab from the same actor/actress
                    otherNegativeListIdx = np.random.choice(len(fileNames))
                otherNegativeIdx = np.random.choice(len(fileNames[otherNegativeListIdx]))
                otherNegative = self.preprocess_image(fileNames[otherNegativeListIdx][otherNegativeIdx])

                pairImages.append([currentImage, otherNegative])
                pairLabels.append(0)

        return np.array(pairImages), np.array(pairLabels)


    def get_random_excluding(self, excluded_actor, all_actors):
        all_actors.remove(excluded_actor)
        return random.choice(random.choice(all_actors))

    def build_siamese_model(self, inputShape, embeddingDim = 64, fineTune = False):
        inputs = tf.keras.layers.Input(inputShape)
        
        # Base being the VGG19 with pre-trained weights
        base_model = tf.keras.applications.vgg19.VGG19(input_shape = inputShape, include_top=False, weights='imagenet')

        # Fine tune phase where we can pick the last % of layers to be updated with our new training data.
        if fineTune == False:
            base_model.trainable=False
        else:
            base_model.trainable = True
            # Fine-tune from this layer onwards
            fine_tune_at = len(base_model.layers) - int(len(base_model.layers)*.10)
            for layer in base_model.layers[:fine_tune_at]:
                layer.trainable = False

        x = base_model(inputs)

        x=tf.keras.layers.GlobalAveragePooling2D()(x)
        outputs=tf.keras.layers.Dense(embeddingDim)(x)
        model = tf.keras.Model(inputs, outputs)

        # return the model to the calling function
        return model

    def euclidean_distance(self, vectors):
        # unpack the vectors into separate lists
        (featsA, featsB) = vectors

        # compute the sum of squared distances between the vectors
        sumSquared = K.sum(K.square(featsA - featsB), axis=1, keepdims=True)

        # return the euclidean distance between the vectors (special case for when the distance is 0)
        return K.sqrt(K.maximum(sumSquared, K.epsilon()))

    def contrastive_loss(self, y, preds, margin=1):
        # explicitly cast the true class label data type to the predicted
        # class label data type (otherwise we run the risk of having two
        # separate data types, causing TensorFlow to error out)
        y = tf.cast(y, preds.dtype)

        # calculate the contrastive loss between the true labels and
        # the predicted labels
        squaredPreds = K.square(preds)
        squaredMargin = K.square(K.maximum(margin - preds, 0))
        loss = K.mean(y * squaredPreds + (1 - y) * squaredMargin)

        # return the computed contrastive loss to the calling function
        return loss

    
    def preprocess_image(self, filename):
        """
        Load the specified file as a JPEG image, preprocess it and
        resize it to the target shape.
        """

        image = Image.open(IMAGES_FOLDER + filename)
        image = image.resize((IMG_SHAPE[0], IMG_SHAPE[1]))
        data = np.asarray(image)
        data = data.astype(np.uint8)
        data = data / 255.0

        return data
    
    def unison_shuffled_copies(self, a, b):
        assert len(a) == len(b)
        p = np.random.permutation(len(a))
        return a[p], b[p]

    def plot_training(self, H, plotPath):
        # construct a plot that plots and saves the training history
        plt.style.use("ggplot")
        plt.figure()
        plt.plot(H.history["loss"], label="train_loss")
        plt.plot(H.history["val_loss"], label="val_loss")
        plt.title("Training Loss")
        plt.xlabel("Epoch #")
        plt.ylabel("Loss")
        plt.legend(loc="lower left")
        plt.savefig(plotPath)

    '''
        Saves the current iteration to a file.
    '''
    def save_siamese(self, name):
        self.model.save(MODEL_FOLDER + name)

    '''
        Loads a siamese network from a file.
    '''
    def load_siamese(self, name):
        self.model = load_model(MODEL_FOLDER + name, custom_objects={'euclidean_distance': self.euclidean_distance, 
                                                                     'contrastive_loss' : self.contrastive_loss})

    '''
        Used to predict whether we have a person in the image.
    '''
    def predict(self, pathToImgA, pathToImgB):
        imgA = self.preprocess_image(pathToImgA)
        imgB = self.preprocess_image(pathToImgB)

        # create a copy of both the images for visualization purpose
        origA = imgA.copy()
        origB = imgB.copy()

        imgA = np.expand_dims(imgA, axis=0)
        imgB = np.expand_dims(imgB, axis=0)   

        preds = self.model.predict([imgA, imgB])
        prob = preds[0][0]

        return prob


    def predict_class(self, testingImage):
        maxSimilarity = 0.0
        maxSimilarityLabel = ""
        imagePaths, labels = self.get_filelist()
        for i in range(len(imagePaths)):
            for originalImage in imagePaths[i]:
                prob = self.predict(originalImage, testingImage)
                if prob > maxSimilarity:
                    maxSimilarity = prob
                    maxSimilarityLabel = labels[i]
        return maxSimilarity, maxSimilarityLabel




    def test_input(self, imgA, imgB, label, filename):
        origA = imgA.copy()
        origB = imgB.copy()

        i = 0
        fig = plt.figure("Pair #{}".format(i + 1), figsize=(4, 2))
        plt.suptitle("Label: {:.2f}".format(label))
        # show first image
        ax = fig.add_subplot(1, 2, 1)
        plt.imshow(origA, cmap=plt.cm.gray)
        plt.axis("off")
        # show the second image
        ax = fig.add_subplot(1, 2, 2)
        plt.imshow(origB, cmap=plt.cm.gray)
        plt.axis("off")
        # show the plot
        plt.savefig(filename + ".png")
