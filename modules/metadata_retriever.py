import requests

'''
    Metadata Retriever
    Main features:
        - Using the movie name, returns the cast list

'''

'''
    Input:
        movie -> string with name of the movie
        year -> integer with the movie release year to better specify

    Returns: list of strings containing main cast list 
'''
def getIdWithName(movie, year, token):
    api_url = "http://api.themoviedb.org/3/search/movie?query={}&year={}&api_key={}".format(movie, year, token)
    movie_list = requests.get(api_url).json()["results"]
    if len(movie_list) >= 1:
        most_popular = max(range(len(movie_list)), key=lambda index: int(movie_list[index]["popularity"]))
        movie_id = movie_list[most_popular]["id"]
        return getCastWithID(movie_id, token)
    return False


'''
    Input:
        id -> integer with movie ID from "themoviedb.org"/TMDb

    Returns: list of strings containing main cast list 
'''
def getCastWithID(id, token):
    api_url = "http://api.themoviedb.org/3//movie/{}/credits?api_key={}".format(id, token)
    cast_list = requests.get(api_url).json()["cast"]
    return [actor["name"] for actor in cast_list if actor["known_for_department"] == "Acting"]



