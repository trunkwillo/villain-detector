# importing the necessary libraries
import cv2
import numpy as np
 
'''
    Input:
        movie_path -> path to the mp4 movie

    Returns: graph containing interactions
'''
def process_movie(movie_path, snn, frame_skip = 24):
    # Creating a VideoCapture object to read the video
    cap = cv2.VideoCapture(movie_path)
    
    
    # Loop until the end of the video
    skip_count = 0
    while (cap.isOpened()):
        # Skipping some frames for increased performance
        skip_count += 1
        if skip_count == frame_skip:
            skip_count = 0

            # Capture frame-by-frame
            ret, frame = cap.read()
            
            faces = get_faces(frame)

    
    # release the video capture object
    cap.release()

'''
    Input:
        frame -> OpenCV2 Image Object containing a single movie frame

    Returns: list of images containing the faces in a certain size
'''
def get_faces(frame):
    face_cascade = cv2.CascadeClassifier('assets/haarcascade_frontalface_default.xml')
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces_coordinates = face_cascade.detectMultiScale(gray, 1.3, 4)
    faces = []
    for f in faces_coordinates:
        (x, y, w, h) = f
        cropped_image = img[y:y+h, x:x+w]
        cropped_image = cv2.resize(cropped_image, (256,256))
        faces.append(cropped_image)
    return faces