from bing_image_downloader import downloader
import os, shutil
import glob
import cv2

'''
    Crawler
    Main features:
        - Get an actor/actress name and create an images dataset with it
        - Verifies whether is a group photo and cuts the face of the actor on a fixed aspect ratio
        - Converts to a specific resolution

'''

'''
    Input:
        cast -> list containing the main cast names
        exclusivity -> string to add to all the search queries
        amount -> Integer that limits the amount of images retrieved

        
    Returns: boolean indicating whether the task was successful
'''
def generateDataset(cast, exclusivity = False, amount = 50):
    try:
        shutil.rmtree('dataset_cache')
    except:
        os.mkdir('dataset_cache')

    for c in cast:
        downloader.download(c, limit=amount,  output_dir='dataset_cache', adult_filter_off=True, force_replace=False, timeout=60)
        cleanDataset(c)
    return True

'''
    Input:
        folder_name -> folder with images to verify

    Returns: integer containing the amount of images removed
'''

def cleanDataset(folder_name):
    face_cascade = cv2.CascadeClassifier('assets/haarcascade_frontalface_default.xml')
    image_paths = glob.glob("dataset_cache/" + folder_name + "/*")
    for i in image_paths:
        if not (".jpg" in i):
            os.remove(i)
            continue
        img = cv2.imread(i)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        faces = face_cascade.detectMultiScale(gray, 1.3, 4)
        if len(faces) != 1:
            os.remove(i)
        else:
            (x, y, w, h) = faces[0]
            cropped_image = img[y:y+h, x:x+w]
            cropped_image = cv2.resize(cropped_image, (256,256))
            cv2.imwrite(i, cropped_image)
    return

